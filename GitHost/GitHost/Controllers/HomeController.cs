﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using GitHost.Data;
using GitHost.Models;
using GitHost.Models.ManageViewModels;
using GitHost.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using IndexViewModel = GitHost.ViewModels.IndexViewModel;

namespace GitHost.Controllers {
    [Authorize]
    public class HomeController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;
        private Project SelectedProject { get; set; }

        public HomeController(ApplicationDbContext context, UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _userManager = userManager;
        }
        
        public IActionResult Index ()
        {
            return View (GetIndexViewModel(GetLoggedInUser()));
        }

        [HttpGet]
        public IActionResult GetContributors(string projectName) {
            try
            {
                
                var selectedProject = from c in _context.Contributors
                    where c.Project.Name == projectName
                    select c.Project;

                var users = from c in _context.Contributors
                            where c.Project.Id == selectedProject.ToList()[0].Id
                    select c.ApplicationUser;

                var usersToAdd = from a in _context.Users
                                select a;
                List<ApplicationUser> userOnProject = users.ToList();
                List<ApplicationUser> userToAddToProject = usersToAdd.ToList();

                var temp = new List<ApplicationUser>();

                foreach (var tempUser in userToAddToProject)
                {
                    foreach (var user in userOnProject)
                    {
                        if (tempUser.Id == user.Id)
                            temp.Add(tempUser);
                    }
                }

                foreach (var tempUser in temp)
                {
                    userToAddToProject.Remove(tempUser);
                }


                IndexViewModel vm = new IndexViewModel();;
                vm.ContributorsListViewModel.UserList = userOnProject;
                vm.ContributorsListViewModel.UsersToAddList = userToAddToProject;
                return PartialView ("_ContributorsContentArea", vm);
            } catch (Exception ex) {
                return BadRequest (ex.Message);
            }

        }

        [HttpPost]
        public IActionResult SaveUserToContributorList(string userId, int projectId)
        {
            try
            {
                SelectedProject = _context.Projects.Find(projectId);
                var selectedUser = _context.Users.Find(userId);

                Contributor contributor = new Contributor()
                {
                    ApplicationUser = selectedUser,
                    Project = SelectedProject
                };
               
                _context.Contributors.Add(contributor);
                _context.SaveChanges();

                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

        }

        [HttpPost]
        public IActionResult CreateProject (IndexViewModel vm)
        {
            ApplicationUser loggedInUser = GetLoggedInUser();

            Project project = new Project()
            {
                Owner = loggedInUser,
                Description = vm.ProjectViewModel.Description,
                Name = vm.ProjectViewModel.Name
            };

            Contributor contributor = new Contributor()
            {
                ApplicationUser = loggedInUser,
                Project = project
            };

            var value = new Dictionary<string, string>
            {
                {"Name", project.Name}
            };

            var postData = new FormUrlEncodedContent(value);

            Program.PostRequest("https://40.87.2.46/add_project.php", postData);

            _context.Projects.Add(project);
            _context.Contributors.Add(contributor);
            _context.SaveChanges();

            return View ("Index", GetIndexViewModel(GetLoggedInUser()));
        }

        private ProjectListViewModel GetUsersProjects(ApplicationUser owner)
        {
            ProjectListViewModel projectListViewModel = new ProjectListViewModel();

            var projectsToDisplay = from c in _context.Contributors
                where c.ApplicationUserId == owner.Id
                select c.Project;

            projectListViewModel.ProjectList = projectsToDisplay.ToList();

            return projectListViewModel;
        }

        private ApplicationUser GetLoggedInUser()
        {
            ApplicationUser loggedInUser = null;

            if (HttpContext != null)
                loggedInUser = _userManager.GetUserAsync(HttpContext.User).Result;

            return loggedInUser;
        }

        public IActionResult SelectProject(int projectId)
        {
            Project p = _context.Projects.Find(projectId);
            SelectedProject = p;

            return View("Index", GetIndexViewModel(GetLoggedInUser()));
        }

        private IndexViewModel GetIndexViewModel(ApplicationUser loggedInUser)
        {
            var owner = GetLoggedInUser();
            IndexViewModel viewModel = new IndexViewModel();
            ProjectListViewModel projectListViewModel = null;
 
            projectListViewModel = GetUsersProjects(loggedInUser);
            viewModel.ProjectListView = projectListViewModel;
            FilesViewModel filesViewModel = new FilesViewModel();
            filesViewModel.Html = "<div></div>";
            viewModel.FilesViewModel = filesViewModel;

            if (SelectedProject == null)            
                SelectedProject = GetDefaultProject(loggedInUser);
            
            if(SelectedProject == null)            
                return viewModel;
            try
            {
                filesViewModel.Html =
                    Program.GetRequest("https://40.87.2.46/gitweb/?p=" + SelectedProject.Name + ".git;a=tree");
            }
            catch (Exception ex)
            {
            }

            viewModel.ProjectViewModel.Name = SelectedProject.Name;
            viewModel.ProjectViewModel.Id = SelectedProject.Id;
            return viewModel;
        }

        private IndexViewModel GetIndexViewModel(ApplicationUser loggedInUser, FilesViewModel filesViewModel)
        {
            var owner = GetLoggedInUser();
            IndexViewModel viewModel = new IndexViewModel();
            ProjectListViewModel projectListViewModel = null;

            projectListViewModel = GetUsersProjects(loggedInUser);
            viewModel.ProjectListView = projectListViewModel;
            viewModel.FilesViewModel = filesViewModel;

            if (SelectedProject == null)
                SelectedProject = GetDefaultProject(loggedInUser);

            if (SelectedProject == null)
                return viewModel;

            viewModel.ProjectViewModel.Name = SelectedProject.Name;
            viewModel.ProjectViewModel.Id = SelectedProject.Id;
            return viewModel;
        }

        private Project GetDefaultProject(ApplicationUser loggedInUser)
        {
            var p = from c in _context.Contributors
                where c.ApplicationUserId == loggedInUser.Id
                select c.Project;
            if (p.ToList().Count <= 0)
                return null;

            return p.ToList()[0]; 

        }

        [HttpGet]
        public IActionResult GetFile(string projectName, string filePath)
        {
            FilesViewModel filesViewModel = new FilesViewModel();
            filesViewModel.Html = "<div></div>";

            var owner = GetLoggedInUser();
            IndexViewModel viewModel = new IndexViewModel();
            ProjectListViewModel projectListViewModel = null;

            projectListViewModel = GetUsersProjects(owner);
            viewModel.ProjectListView = projectListViewModel;
            

            if (SelectedProject == null)
                SelectedProject = GetDefaultProject(owner);

            //if (SelectedProject == null)
            //    return viewModel;

            viewModel.ProjectViewModel.Name = SelectedProject.Name;
            viewModel.ProjectViewModel.Id = SelectedProject.Id;

            try
            {
                filesViewModel.Html =
                    Program.GetRequest("https://40.87.2.46/gitweb/?p=" + projectName + "a=tree;f=" + filePath);
            }
            catch (Exception ex)
            {
            }
            viewModel.FilesViewModel = filesViewModel;

            return View("Index", GetIndexViewModel(GetLoggedInUser(), filesViewModel));
        }
    }
}