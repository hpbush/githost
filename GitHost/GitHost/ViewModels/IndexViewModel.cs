﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GitHost.Models;

namespace GitHost.ViewModels
{
    public class IndexViewModel
    {
        public ProjectListViewModel ProjectListView { get; set; }
        public ProjectViewModel ProjectViewModel { get; set; }
        public ContributorsListViewModel ContributorsListViewModel { get; set; }
        public FilesViewModel FilesViewModel { get; set; }

        public IndexViewModel()
        {
            ContributorsListViewModel = new ContributorsListViewModel();
            ProjectViewModel = new ProjectViewModel();
        }

        
    }
}
