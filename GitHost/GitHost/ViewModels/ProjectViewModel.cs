﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GitHost.Models;

namespace GitHost.ViewModels
{
    public class ProjectViewModel
    {
        public int Id { get; set; }
        public ApplicationUser Owner { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
