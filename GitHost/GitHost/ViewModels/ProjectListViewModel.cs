﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GitHost.Models;

namespace GitHost.ViewModels
{
    public class ProjectListViewModel
    {
        public List<Project> ProjectList { get; set; }
    }
}
