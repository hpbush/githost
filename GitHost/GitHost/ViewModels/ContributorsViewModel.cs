﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GitHost.Models;

namespace GitHost.ViewModels
{
    public class ContributorsListViewModel
    {

        public List<ApplicationUser> UsersToAddList { get; set; }
        public List<ApplicationUser> UserList { get; set; }

        public ContributorsListViewModel()
        {

            UserList = new List<ApplicationUser>();
            UsersToAddList = new List<ApplicationUser>();
        }
    }

             
    
}