﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using GitHost.Models.AccountViewModels;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace GitHost
{
    public class Program
    {
        public static void Main(string[] args)
        {
            BuildWebHost(args).Run();
        }

        public static IWebHost BuildWebHost(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>()
                .Build();


        public static HttpClient GetHttpClient()
        {

            var httpClientHandler = new HttpClientHandler();
            httpClientHandler.ServerCertificateCustomValidationCallback = (message, cert, chain, errors) =>
            {
                return true;
            };
           return new HttpClient(httpClientHandler);
        }

        public static async void PostRequest(string uri, FormUrlEncodedContent postData)
        {
            var myUrl = new Uri(uri);
            var client = GetHttpClient();
            await client.PostAsync(myUrl, postData);
        }

        public static string GetRequest(string uri)
        {
            var myUrl = new Uri(uri);
            var client = GetHttpClient();
            return client.GetStringAsync(myUrl).Result;
        }

    }
}

