﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GitHost.Models
{
    public class Contributor
    {
        public int Id { get; set; }

        public Project Project { get; set; }
        public int ProjectId { get; set; }

        public ApplicationUser ApplicationUser { get; set; }
        public string ApplicationUserId { get; set; }
    }
}
