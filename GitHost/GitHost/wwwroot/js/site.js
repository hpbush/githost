﻿// Write your JavaScript code.
$(document).ready(function () {
    
    $(".projectTab").hide();
    $("#tabs ul li:first").attr("id", "current");
    $("#pageContentArea div:first").fadeIn();

    $("#tabs ul li a").click(function(e) {
        e.preventDefault();
        if ($(this).attr("id") === "current") {
            return;
        } else {
            $(".projectTab").hide();
            $("#tabs ul li").attr("id", "");
            $(this).parent().attr("id", "current");
            $($(this).attr('href')).fadeIn();
        }
    });

    var table = $('.tree')[0];

    $(".tree tr").click(function (e) {
        //alert($(this).children("td").html());
        var target = e.target;
        //console.log(target.innerHTML);
        var projectName = document.getElementById("ProjectTitle").getAttribute("data-id") + ".git;";
        var filePath = document.getElementById("FilePath").innerHTML + "/" + target.innerHTML;
        if (filePath.charAt(0) === "/")
            filePath = filePath.substr(1);
        console.log(filePath);
        

        $.ajax({
            type: "Get",
            url: "/Home/GetFile",
            contentType: "application/json; charset=utf-8",
            data: { ProjectName: projectName , FilePath: filePath},
            dataType: "html",
            cache: false,
            success: function (data) {
                //build table
                //$("#fileContentArea").innerHTML = data;

                //var placeholder = document.createElement("html");
                //placeholder.innerHtml = data;

                console.log(data[1].data);

                //$("#fileContentArea").html(placeholder);
            },
            error: function (data) {
                alert("failed to build table");
            }

        });


    });

    table.classList.add("table");
    table.classList.add("table-hover");

    var tableBody = table.firstElementChild;
    console.log(tableBody);



    for (var i = 0; i < tableBody.children.length; i++) {
    
        var fileName = tableBody.children[i].firstElementChild.firstElementChild.text;
        tableBody.children[i].innerHTML = "";
        var customRowData = document.createElement("td");
        customRowData.innerHTML = "<a href=\"javascript: void (0);\">" + fileName + "</a>";
        tableBody.children[i].appendChild(customRowData);
    }

});

$("#contributorsTab").on("click", function() {
    refreshContributorContentArea();
});

function buildTable() {

}

function refreshContributorContentArea() {
    var projectName = document.getElementById("ProjectTitle").getAttribute("data-id");
   
    $.ajax({
        type: "Get",
        url: "/Home/GetContributors",
        data: {ProjectName : projectName},
        contentType: "application/json; charset=utf-8",
        dataType: "html",
        cache: false,
        success: function (data) {
            $("#contributorsContentArea").html(data);
        },
        error: function(data) {
            
        }

    });
};


function addContributor() {
    var projectId = document.getElementById("ProjectTitle").getAttribute("data-value");
    
    var userid = $("#users option[value='" + $('#contributorSearch').val() + "']").attr('data-value');

    $.ajax({
        type: "Post",
        url: "/Home/SaveUserToContributorList",
        data: { UserId: userid, ProjectId: projectId},
        dataType: "html",
        cache: false,
        success: function (data) {
            refreshContributorContentArea();

        },
        error: function(data) {
            alert("Error adding contributor");
            console.log(data.toString());
        }

    });
};


$('.projects').click(function (e) {


    //let project = e.target  ** Let was throwing an error. ** 
    console.log(project.innerText);


    $.ajax({
        url: 'https://40.87.2.46/a.php',
        headers: {
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',

        },
        success: function(result) {
            console.log(result);
        }
    });

});

$('.tree').click(function(e) {

});

$('.search, .header, .page_header, .page_nav, .page_footer, .mode, .size').remove();
$('.tree').addClass('.table-hover mt-2')